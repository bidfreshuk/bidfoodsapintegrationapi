﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAPBidFoodAPI.Models
{
    public class Error
    {

        public string ErrorCode { get; set; }

        public string ErrorRef { get; set; }

        public string ErrorMsg { get; set; }
    }
}