﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAPBidFoodAPI.Models
{
    public class ProcessedOrder
    {

        public ProcessedOrder()
        {
           // ProcessedOrderLineItems = new List<ProcessedOrderLineItem>();
            //  Version = 1.0m;
        }

        //public ProcessedOrder(NewOrder newOrder, SAPbobsCOM.Documents oOrder)
        //{
        //    //there will only be one order inside the list
        //    List<NewOrderLineItem> newOrderLineItems = newOrder.NewOrderLineItems;
        //    ProcessedOrderLineItems = new List<ProcessedOrderLineItem>();
        //    OrderStatusCode = "0";
        //    //  TransactionID = "B1FE1299-CC08-4563-9B91-DCA5FB148130";
        //    foreach (NewOrderLineItem newOrderLine in newOrderLineItems)
        //    {
        //        ProcessedOrderLineItem processedOrderLineItem = new ProcessedOrderLineItem();
        //        processedOrderLineItem.Description = newOrderLine.Description;
        //        processedOrderLineItem.InvUOM = newOrderLine.UOM;
        //        processedOrderLineItem.LineItemNote = newOrderLine.LineItemNote;
        //        processedOrderLineItem.OrderLineNo = newOrderLine.OrderLineNo;
        //        processedOrderLineItem.ProductItemCode = newOrderLine.ProductItemCode;
        //        processedOrderLineItem.OrderLineStatus = "0";
        //        processedOrderLineItem.BrandRef = "";
        //        processedOrderLineItem.PackSizeRef = "";
        //        processedOrderLineItem.CustGLCode1 = "";
        //        processedOrderLineItem.CustGLCode2 = "";
        //        processedOrderLineItem.CustGLCode3 = "";
        //        processedOrderLineItem.CustGLCode4 = "";



        //        //should we return exact available quantity?
        //        //27.10. - no, just ordered quantity
        //        if (newOrder.RecalculatePrice == true)
        //        {
        //            processedOrderLineItem.UnitPrice = newOrderLine.UnitPrice;
        //        }

        //        processedOrderLineItem.QtyDelivered = newOrderLine.QtyOrdered;
        //        processedOrderLineItem.QtyOrdered = newOrderLine.QtyOrdered;
        //        ProcessedOrderLineItems.Add(processedOrderLineItem);


        //    }

        //}



        //public ProcessedOrder(SAPbobsCOM.Documents oOrder)
        //{
        //    //there will only be one order inside the list
        //    //oOrder.Lines
        //    SAPbobsCOM.Document_Lines newOrderLineItems = oOrder.Lines;
        //    ProcessedOrderLineItems = new List<ProcessedOrderLineItem>();
        //    OrderStatusCode = "0";
        //    //  TransactionID = "B1FE1299-CC08-4563-9B91-DCA5FB148130";
        //    //  foreach (SAPbobsCOM.Document_Lines newOrderLine in newOrderLineItems)
        //    //  (SAPbobsCOM.Document_Lines newOrderLine in newOrderLineItems) 
        //    OrderNo = oOrder.DocNum.ToString();
        //    for (int i = 0; i < newOrderLineItems.Count; i++)
        //    {
        //        newOrderLineItems.SetCurrentLine(i);
        //        ProcessedOrderLineItem processedOrderLineItem = new ProcessedOrderLineItem();
        //        processedOrderLineItem.Description = newOrderLineItems.ItemDescription;
        //        processedOrderLineItem.InvUOM = newOrderLineItems.UoMCode;
        //        processedOrderLineItem.LineItemNote = (string)newOrderLineItems.UserFields.Fields.Item("U_Specs").Value;
        //        processedOrderLineItem.OrderLineNo = newOrderLineItems.LineNum;
        //        processedOrderLineItem.ProductItemCode = newOrderLineItems.ItemCode;
        //        processedOrderLineItem.UnitPrice = Convert.ToDecimal(newOrderLineItems.UnitPrice);
        //        processedOrderLineItem.ExtendedPrice = Convert.ToDecimal(newOrderLineItems.LineTotal);
        //        processedOrderLineItem.LineTax = Convert.ToDecimal(newOrderLineItems.TaxTotal);
        //        processedOrderLineItem.OrderLineStatus = "0";
        //        processedOrderLineItem.BrandRef = "";
        //        processedOrderLineItem.PackSizeRef = "";
        //        processedOrderLineItem.CustGLCode1 = "";
        //        processedOrderLineItem.CustGLCode2 = "";
        //        processedOrderLineItem.CustGLCode3 = "";
        //        processedOrderLineItem.CustGLCode4 = "";



        //        //should we return exact available quantity?
        //        //27.10. - no, just ordered quantity
        //        //if (newOrder.RecalculatePrice == true)
        //        //{
        //        //    processedOrderLineItem.UnitPrice = newOrderLine.UnitPrice;
        //        //}

        //        processedOrderLineItem.QtyDelivered = Convert.ToDecimal(newOrderLineItems.Quantity);
        //        processedOrderLineItem.QtyOrdered = Convert.ToDecimal(newOrderLineItems.Quantity);
        //        //ProcessedOrderLineItems.Add(processedOrderLineItem);


        //    }

        //}

        //public int UpdateInfo(SAPbobsCOM.Documents oOrder)
        //{
        //    int Status = 0;

        //    OrderNo = oOrder.DocNum.ToString();
        //    //  proces
        //    return Status;

        //}
        public int DocEntry { get; set; }

        public int DocNum { get; set; } 

        public string WarehouseCode { get; set; }

        public string OrderNo { get; set; }

        public DateTime OrderDate { get; set; }

        public string ConfirmationNo { get; set; }

        public string AccountCode { get; set; }

        public string OrderStatusCode { get; set; }

        public string OurRef { get; set; }

        public string CustRef { get; set; }

        public int NoOfItems { get; set; }

        public decimal? Tax { get; set; }

        public decimal? OrderTotal { get; set; }

        public string OrderNotes { get; set; }

        public string DropNo { get; set; }

        public string RouteNo { get; set; }

      
    }
}