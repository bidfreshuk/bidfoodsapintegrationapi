﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAPBidFoodAPI.Models
{
    public class Enums
    {
    }

    public enum ProcessedOrderListSubmitLoadType
    {

        /// <remarks/>
        Partial,

        /// <remarks/>
        Full,
    }

    public enum ProcessedOrderListSubmitCulture
    {

        /// <remarks/>
        en,
    }
}