﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAPBidFoodAPI.Models
{
    public class OrderHeader
    {
        public string DistributorCustomerIdentifier { get; set; } // EXTID
        public string OrderDate { get; set; }
        public string DistributerOrderReference { get; set; } //NumatCard - ord_no
        public string Street1 { get; set; } // header
        public string Street2 { get; set; }
        public string Block { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string ZipCode { get; set; }
        public string AccountReference { get; set; } // crdcode
        public string DistributerCustomerOrderRef { get; set; } // cust refe
        public string DeliveryDate { get; set; } // deliverydate
        public string DistributorRoute { get; set; } //routenumber
        public string DistributorDropNo { get; set; } // drop no

        public List<OrderLine> Items { get; set; }
    }

    public class OrderLine
    {
        public string OrderRefLineNo { get; set; } //cust order line no // line 
        public string DistributorProductCode { get; set; } // customer product code 
        public string Quantity { get; set; }
        public string UOM { get; set; }
        public string ProductCode { get; set; }
    }


    public class GetAllOrders
    {

        public GetAllOrders()
        {

        }

        public int DocEntry { get; set; }
        public int DocNum { get; set; }
        public string DocStatus { get; set; }
        public DateTime DocDate { get; set; }
        public string AccountCode { get; set; }
        public string Reference { get; set; }
        public double DocTotal { get; set; }
        public DateTime UpdateDate { get; set; }
        public string PickStatus { get; set; }
        public string PO_Number { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string AccountName { get; set; }
        public string Address { get; set; }
        public double VatSum { get; set; }
        public double DiscSum { get; set; }
        public string Confirmed { get; set; }
        public string Cancelled { get; set; }
        public string Status { get; set; }

    }
}