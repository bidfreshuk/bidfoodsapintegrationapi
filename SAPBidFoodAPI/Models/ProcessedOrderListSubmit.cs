﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using SAPBidFoodAPI.Models;

namespace SAPBidFoodAPI.Models
{
    public class ProcessedOrderListSubmit
    {

        public ProcessedOrderListSubmit()
        {
            Errors = new List<Error>();
           // ProcessedOrders = new List<ProcessedOrder>();
            Culture = ProcessedOrderListSubmitCulture.en;
            ErrorCount = 0;
        }


        public ProcessedOrderListSubmit(OrderHeader newOrderHeader, SAPbobsCOM.Documents oOrder, string lastKey, Error oError = null)
        {
            //always only one order
            Version = 1.5m;
            TransactionID = "B1FE1299-CC08-4563-9B91-DCA5FB148130";
            StartTime = DateTime.Now;
            EndTime = DateTime.Now;



            Errors = new List<Error>();
           
            ProcessedOrder processedOrder = new ProcessedOrder();
            ProcessedOrders = new List<ProcessedOrder>();
            processedOrder.AccountCode = newOrderHeader.AccountReference;
            processedOrder.ConfirmationNo = lastKey;
            processedOrder.CustRef = newOrderHeader.DistributorCustomerIdentifier; // customer no
            processedOrder.OurRef = newOrderHeader.DistributerOrderReference; //ordernumber 
            processedOrder.OrderStatusCode = "0";
            processedOrder.OrderNo = oOrder.DocNum.ToString();
            processedOrder.OrderTotal = Convert.ToDecimal(oOrder.DocTotal);
            processedOrder.Tax = Convert.ToDecimal(oOrder.VatSum);
            processedOrder.DropNo = newOrderHeader.DistributorDropNo;
            processedOrder.RouteNo = newOrderHeader.DistributorRoute;

            processedOrder.NoOfItems = oOrder.Lines.Count;

            processedOrder.DocEntry = oOrder.DocEntry;
            processedOrder.DocNum = oOrder.DocNum; 

            ProcessedOrders.Add(processedOrder);
            Culture = ProcessedOrderListSubmitCulture.en;
            ErrorCount = 0;
        }



        

        [XmlAttribute]
        public string TransactionID { get; set; }

        [XmlAttribute]
        public DateTime StartTime { get; set; }

        [XmlAttribute]
        public DateTime EndTime { get; set; }

        [XmlAttribute]
        public decimal Version { get; set; }

        [XmlAttribute]
        public string SiteID { get; set; }

        [XmlAttribute]
        public ProcessedOrderListSubmitLoadType LoadType { get; set; }

        [XmlAttribute]
        public string LoadFilter { get; set; }

       [XmlAttribute]
       public ProcessedOrderListSubmitCulture Culture { get; set; }

        [XmlAttribute]
        public int StatusID { get; set; }

        [XmlAttribute]
        public int ErrorCount { get; set; }

        public List<ProcessedOrder> ProcessedOrders { get; set; }

        public List<Error> Errors { get; set; }
    }
}