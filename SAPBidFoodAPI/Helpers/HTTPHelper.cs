﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace SAPBidFoodAPI.Helpers
{
    public static class HTTPHelper
    {


        public static ServerConnection getConnection()
        {

            ServerConnection connection;
            object con = HttpContext.Current.Application.Get("SAPConnection");
            if(con == null)
            {
                connection = new ServerConnection();

                if (connection.Connect() != 0)
                {
                    LogHelper.Log("0", "connection.Connect() != 0");
                    connection.Connect();
                }
                HttpContext.Current.Application.Add("SAPConnection", connection);
            }
            else
            {
                connection = (ServerConnection)HttpContext.Current.Application.Get("SAPConnection");
                if (connection.Connect() != 0)
                {
                    LogHelper.Log("0", "connection.Connect() != 0");
                    connection.Connect();
                    HttpContext.Current.Application.Add("SAPConnection", connection);
                }
            }

            return connection; 
            
        }

        public static void disconnectConnection()
        {
            ServerConnection connection;
            object con = HttpContext.Current.Application.Get("SAPConnection");
            if (con != null)
            {
                connection = (ServerConnection)HttpContext.Current.Application.Get("SAPConnection");
                connection.Disconnect();   
            }
        }
    }
}