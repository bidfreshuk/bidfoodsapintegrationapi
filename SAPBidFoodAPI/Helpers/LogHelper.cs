﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SAPBidFoodAPI.Helpers
{
    public static class LogHelper
    {
        static string sRuta = @"C:\Temp3";

        public static void Log(string sStatus, string sMsg, string sMethod = "", int iLine = 0)
        {
            try
            {

                Directory.CreateDirectory(sRuta + "\\Logs\\" + sStatus);
                using (StreamWriter w = File.AppendText(sRuta + "\\Logs\\" + sStatus + "\\" + DateTime.Now.ToString("MM-dd-yyyy") + ".txt"))
                {
                    w.WriteLine("Log Entry: {0} {1} - {2}({3})", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString(), sMethod, iLine);
                    w.WriteLine("   {0}", sMsg);
                    w.WriteLine("---------------------------------------------------------------------------------------------------------------");
                }
                if (sStatus != "WARNING") //Do nothing for warnings
                {
                    //  Console.WriteLine(sStatus + " - " + sMsg);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR - " + e.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}