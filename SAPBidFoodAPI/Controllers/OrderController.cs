﻿using SAPBidFoodAPI.Helpers;
using SAPBidFoodAPI.Models;
using System.Collections.Generic;
using System.IO;
using System.Web.Http;
using System.Web;
using System;
using System.Globalization;

namespace SAPBidFoodAPI.Controllers
{
    public class OrderController : ApiController
    {
        // GET: Order
        #region Global Variables 
        //variavbles del programa 
        static SAPbobsCOM.Company oCompany = null;
        #endregion

        [Route("api/Order/AddOrder")]
        [HttpPost]
        public ProcessedOrderListSubmit AddOrder([FromBody]OrderHeader newOrderHeader)
        {

            string lastKey = "";
            //ServerConnection connection = (ServerConnection)HttpContext.Current.Application.Get("SAPConnection");

            ServerConnection connection = HTTPHelper.getConnection();
            if (!connection.Connected())
            {
                LogHelper.Log("ERROR", connection.GetErrorMessage(), "New Order");

            }

            SAPbobsCOM.Documents oOrder;
            var err = new Error();
            SAPbobsCOM.Recordset oRS;
            oCompany = connection.GetCompany();
            oOrder = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders);

            try
            {
               
                if (newOrderHeader != null)
                {

                    LogHelper.Log("Message", "Checking if Order Exists " + newOrderHeader.DistributerOrderReference, "New Order");
                    try
                    {
                        if (!String.IsNullOrEmpty(newOrderHeader.DistributerOrderReference))
                        {
                            oRS = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                            //Get a list of open orders
                            //var strQuerycheck = @"Select DocNum
                            //            FROM ORDR T0
                            //            where NOT T0.NumAtCard IS NULL AND T0.NumAtCard = '" + newOrderHeader.DistributerOrderReference + "'";
                            var strQuerycheck = @"Select DocNum
                                        FROM ORDR T0
                                        where NOT T0.NumAtCard IS NULL AND T0.NumAtCard = '" + newOrderHeader.DistributerOrderReference + "' and ISNULL(T0.U_Route_Number,'') = '" + newOrderHeader.DistributorRoute + "' " +
                                        "and T0.CardCode = '" + newOrderHeader.AccountReference + "'";


                            oRS.DoQuery(strQuerycheck);

                            if (oRS.RecordCount > 0)
                            {
                                string docnum = oRS.Fields.Item("DocNum").Value.ToString();
                                LogHelper.Log("Message", "Order Exists. So continue. DocNum = " + docnum, "New Order");
                                err.ErrorCode = "001";
                                err.ErrorRef = "API - Add BidFood orders";
                                err.ErrorMsg = "Order No. " + newOrderHeader.DistributerOrderReference + " already Exists.";

                                ProcessedOrderListSubmit processedOrderListSubmitt = new ProcessedOrderListSubmit(newOrderHeader, oOrder, lastKey);
                                if (err.ErrorCode != null)
                                {

                                    processedOrderListSubmitt.Errors.Add(err);
                                    processedOrderListSubmitt.ProcessedOrders[0].OrderStatusCode = "999";
                                }

                                return processedOrderListSubmitt;


                            }
                            else
                            {
                                LogHelper.Log("Message", "Order Doesn't Exists. Continue with the order", "New Order");
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        LogHelper.Log("Message", "Error checking if Order Exists " + ex.Message, "New Order");

                    }


                    oOrder.UserFields.Fields.Item("U_EXT_ID").Value = newOrderHeader.DistributorCustomerIdentifier; // customerno -sheet

                    oOrder.CardCode = newOrderHeader.AccountReference; 
                    oOrder.NumAtCard = newOrderHeader.DistributerOrderReference; //ordernumber -sheet

                    //oOrder.SUBCATNUM = newOrder.SUBCATNUM;
                    string docdate = newOrderHeader.DeliveryDate;
                    string result = DateTime.ParseExact(docdate, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                    oOrder.DocDueDate = Convert.ToDateTime(result);


                   // oOrder.Series = 66; // Meat --for production = 3 |66
                     oOrder.Series = 66; // Meat --for production = 66

                    //oOrder.Series = 3;//production

                    if (Convert.ToDateTime(result).Hour > 0)
                    {
                        oOrder.UserFields.Fields.Item("U_DeliverTo").Value = ((Convert.ToDateTime(result).Hour)).ToString() + ":00";
                        oOrder.UserFields.Fields.Item("U_DeliverFrom").Value = ((Convert.ToDateTime(result).Hour - 2)).ToString() + ":00";
                    }
                    else
                    {
                        oOrder.UserFields.Fields.Item("U_DeliverTo").Value = "23" + ":59";
                        oOrder.UserFields.Fields.Item("U_DeliverFrom").Value = "00" + ":01";
                    }

                    if (newOrderHeader.Items.Count > 0) {
                        foreach (var obj in newOrderHeader.Items) {
                            /* Dated: 07/11/2019 - removing leading zero - temprory fix*/ 
                            if (obj.DistributorProductCode.StartsWith("0"))
                            {
                                int _str = Convert.ToInt32(obj.DistributorProductCode);
                                obj.DistributorProductCode = Convert.ToString(_str);
                            }
                            oOrder.Lines.SupplierCatNum = obj.DistributorProductCode;

                            oOrder.Lines.Quantity = Convert.ToDouble(obj.Quantity);

                            oOrder.Lines.UserFields.Fields.Item("U_Original_Qty").Value = Convert.ToDouble(obj.Quantity);

                            oOrder.Lines.ExLineNo = obj.OrderRefLineNo;

                            oOrder.Lines.WarehouseCode = "200";

                            oRS = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset); // because we dnt have productitemcode
                            ////Get a list of open orders
                            string strQuery1 = @"Select ISNULL(T0.U_PICKING_AREA,'') as PickingArea
	                                ,ISNULL(T0.U_SAP_TC,0) as TC  
                                    from OITW T0 
                                    INNER JOIN OSCN T1 ON T1.ItemCode = T0.ItemCode
                                    WHERE T1.Substitute = '" + obj.DistributorProductCode + "'" +
                                    "and T1.CardCode = '"+ oOrder.CardCode +"'"+
                                    " and T0.WhsCode = '200'";


                            oRS.DoQuery(strQuery1);

                            if (oRS.RecordCount > 0)
                            {
                                oOrder.Lines.UserFields.Fields.Item("U_SAP_TC").Value = oRS.Fields.Item("TC").Value;
                                oOrder.Lines.UserFields.Fields.Item("U_PICK_AREA").Value = oRS.Fields.Item("PickingArea").Value;

                            }

                            oOrder.Lines.Add();
                        }
                    }

                  

                    if (!String.IsNullOrEmpty(newOrderHeader.DistributerCustomerOrderRef.Trim()))
                    {
                        oOrder.UserFields.Fields.Item("U_PO_NUMBER").Value = newOrderHeader.DistributerCustomerOrderRef;
                    }

                    if (!String.IsNullOrEmpty(newOrderHeader.DistributorDropNo.Trim()))
                    {
                        oOrder.UserFields.Fields.Item("U_DROPNO").Value = newOrderHeader.DistributorDropNo;  
                    }

                    if (!String.IsNullOrEmpty(newOrderHeader.DistributorRoute.Trim()))
                    {
                        oOrder.UserFields.Fields.Item("U_Route_Number").Value = newOrderHeader.DistributorRoute;
                    }

                 


                    oOrder.AddressExtension.ShipToStreet = newOrderHeader.Street1;
                    oOrder.AddressExtension.ShipToStreetNo = newOrderHeader.Street2;
                    oOrder.AddressExtension.ShipToAddress2 = newOrderHeader.Block;

                    //oOrder.AddressExtension.ShipToAddress3 = newOrder.;
                    oOrder.AddressExtension.ShipToCity = newOrderHeader.City;
                    oOrder.AddressExtension.ShipToZipCode = newOrderHeader.ZipCode;
                    oOrder.AddressExtension.ShipToCounty = newOrderHeader.County;

                }

               
                int lRetCode = oOrder.Add();
                if (lRetCode == 0)
                {
                    lastKey = oCompany.GetNewObjectKey();
                    // fill in created order info
                    if (oOrder.GetByKey(Convert.ToInt32(lastKey)))
                    {
                        //  create returning document while connecting to SAP
                        //  ProcessedOrderListSubmit processedOrderListSubmit = new ProcessedOrderListSubmit(newOrderListSubmit, oOrder, lastKey);
                        //  iRetCode =  processedOrderListSubmit.UpdateInfo(oOrder);
                    }
                }
                else
                {
                    LogHelper.Log("ERROR", oCompany.GetLastErrorDescription(), "NewOrder");
                    throw new Exception(oCompany.GetLastErrorDescription());
                }
               
            }
               
            catch (Exception ex)
            {

                {
                    err.ErrorCode = "002";
                    err.ErrorRef = "API - Add BidFood orders";
                    err.ErrorMsg = ex.Message;
                };
            }
            finally
            {
                //oCompany.Disconnect();
            }

            //create  list to return 
            ProcessedOrderListSubmit processedOrderListSubmit = new ProcessedOrderListSubmit(newOrderHeader, oOrder, lastKey);
            if (err.ErrorCode != null)
            {
                //add error info to returning message
                processedOrderListSubmit.Errors.Add(err);
                //set status to 999, error code
               processedOrderListSubmit.ProcessedOrders[0].OrderStatusCode = "999";
            }
            // System.Runtime.InteropServices.Marshal.ReleaseComObject(connection.GetCompany());
            // connection.GetCompany().Disconnect();
            // oCompany.Disconnect();
            return processedOrderListSubmit;
        }

        [Route("api/Order/GetOrder")] // test function with fixed values
        [HttpGet]
        public List<GetAllOrders> GetOpenOrders()
        {
            SAPbobsCOM.Company oCompany = null;
            string cardcode = "BIDV035";
            if (cardcode.IndexOf("/") > 0)
            {
                cardcode = cardcode.Substring(0, cardcode.IndexOf("/"));
            }

            ServerConnection connection = HTTPHelper.getConnection();
            if (!connection.Connected())
            {
                // LogHelper.Log("ERROR", connection.GetErrorMessage(), "New Order");

            }
            SAPbobsCOM.Documents oOrder;
            string strQuery = "";

            //var err = new Error();

            oCompany = connection.GetCompany();

            try
            {
                SAPbobsCOM.Recordset oRS;
                oRS = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                //Get a list of open orders
                strQuery = "SELECT T0.DocEntry, T0.DocNum, T0.DocStatus, T0.DocDate, T0.CardCode, T0.NumAtCard, T0.DocTotal, T0.UpdateDate, T0.PickStatus, '' as PO_Number,T0.DocDueDate, T0.CardName, T0.Address, T0.VatSum, T0.DiscSum, T0.Confirmed, T0.CANCELED, '' as Status FROM ORDR T0 WHERE(T0.DocStatus = 'O' and T0.CardCode ='" + cardcode + "')";
                oRS.DoQuery(strQuery);
                List<GetAllOrders> getOrders = new List<GetAllOrders>();
                if (oRS.RecordCount > 0)
                {
                    do
                    {
                        GetAllOrders openOrder = new GetAllOrders();
                        openOrder.DocEntry = oRS.Fields.Item("DocEntry").Value;
                        openOrder.DocNum = oRS.Fields.Item("DocNum").Value;
                        openOrder.DocStatus = oRS.Fields.Item("DocStatus").Value;
                        openOrder.DocDate = oRS.Fields.Item("DocDate").Value;
                        openOrder.AccountCode = oRS.Fields.Item("CardCode").Value;
                        openOrder.Reference = oRS.Fields.Item("NumAtCard").Value;
                        openOrder.DocTotal = oRS.Fields.Item("DocTotal").Value;
                        openOrder.UpdateDate = oRS.Fields.Item("UpdateDate").Value;
                        openOrder.PickStatus = oRS.Fields.Item("PickStatus").Value;
                        openOrder.PO_Number = oRS.Fields.Item("PO_Number").Value;
                        openOrder.DeliveryDate = oRS.Fields.Item("DocDueDate").Value;
                        openOrder.AccountName = oRS.Fields.Item("CardName").Value;
                        openOrder.Address = oRS.Fields.Item("Address").Value;
                        openOrder.VatSum = oRS.Fields.Item("VatSum").Value;
                        openOrder.DiscSum = oRS.Fields.Item("DiscSum").Value;
                        openOrder.Confirmed = oRS.Fields.Item("Confirmed").Value;
                        openOrder.Cancelled = oRS.Fields.Item("CANCELED").Value;
                        openOrder.Status = oRS.Fields.Item("Status").Value;
                        getOrders.Add(openOrder);
                        oRS.MoveNext();
                    }
                    while (!oRS.EoF);
                }
                //no more processing, return info
                return getOrders;
            }
            catch (Exception ex)
            {

                //{
                //    err.ErrorCode = "505";
                //    err.ErrorRef = "API - Get customer orders";
                //    err.ErrorMsg = ex.Message;
                //};
            }
            finally
            {
                //oCompany.Disconnect();
            }

            //create empty list to return in case of error
            return new List<GetAllOrders>();

        }

        
    }
}